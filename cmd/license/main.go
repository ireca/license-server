package main

import (
	"context"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/license-server/config"
	"gitlab.com/ireca/license-server/internal/handlers"
	factory2 "gitlab.com/ireca/license-server/internal/handlers/auth/factory"
	"gitlab.com/ireca/license-server/internal/repositories/factory"
	"gitlab.com/ireca/license-server/internal/server"
	"gitlab.com/ireca/license-server/internal/utils"
	"gopkg.in/Graylog2/go-gelf.v1/gelf"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	buildVersion string
	buildDate    string
	buildCommit  string
)

func main() {
	fmt.Printf("Build version:%s\n", buildVersion)
	fmt.Printf("Build date:%s\n", buildDate)
	fmt.Printf("Build commit:%s\n", buildCommit)

	configFromFile, err := config.LoadConfigFile("config.json")
	if err != nil {
		utils.LogFatalError("i can't load configuration file: %v", err)
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		utils.LogFatalError("Can't read config: %v", err)
	}

	ctx := context.Background()
	if cfg.Vault.IsRun {
		clientVault, errV := vault.New(
			vault.WithAddress(cfg.Vault.URL),
			vault.WithRequestTimeout(time.Duration(cfg.Vault.RequestTimeout)*time.Second),
		)
		if errV != nil {
			utils.LogFatalError("can't connect to the Vault service: %v", errV)
		}

		resp, errV := clientVault.Auth.AppRoleLogin(
			ctx,
			schema.AppRoleLoginRequest{
				RoleId:   cfg.Vault.RoleId,
				SecretId: cfg.Vault.SecretId,
			},
		)
		if errV != nil {
			utils.LogFatalError("can't get a response from the Vault service: %v", errV)
		}

		if errV = clientVault.SetToken(resp.Auth.ClientToken); errV != nil {
			utils.LogFatalError("i can't set a token for the Vault service: %v", errV)
		}

		vaultResp, errV := clientVault.Secrets.KvV2Read(ctx, "license-service", vault.WithMountPath(cfg.Vault.MountPath))
		if errV != nil {
			utils.LogFatalError("i can't get a secrets from the Vault service: %v", errV)
		}

		cfg, errV = config.GetConfigByVault(*cfg, vaultResp)
		if errV != nil {
			utils.LogFatalError("i can't update config parameter from the Vault service: %v", errV)
		}

		utils.LogInfo("successful connect to the Vault and got secrets!")
	}

	if cfg.Sentry.IsRun {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:                cfg.Sentry.Dsn,
			Debug:              cfg.Sentry.Debug,
			AttachStacktrace:   cfg.Sentry.AttachStacktrace,
			SampleRate:         cfg.Sentry.SampleRate,
			EnableTracing:      cfg.Sentry.EnableTracing,
			TracesSampleRate:   cfg.Sentry.TracesSampleRate,
			IgnoreErrors:       cfg.Sentry.IgnoreErrors,
			IgnoreTransactions: cfg.Sentry.IgnoreTransactions,
			SendDefaultPII:     cfg.Sentry.SendDefaultPII,
			ServerName:         cfg.Sentry.ServerName,
			Release:            cfg.Sentry.Release,
			Dist:               cfg.Sentry.Dist,
			Environment:        cfg.Sentry.Environment,
			MaxBreadcrumbs:     cfg.Sentry.MaxBreadcrumbs,
			MaxSpans:           cfg.Sentry.MaxSpans,
			MaxErrorDepth:      cfg.Sentry.MaxErrorDepth,
		})
		if err != nil {
			utils.LogFatalError("can't connect to the Sentry service: %v", err)
		}
		defer func() {
			if errR := recover(); errR != nil {
				sentry.CurrentHub().Recover(errR)
				sentry.Logger.Printf("panic recovered: %v", errR)
				sentry.Flush(time.Second * 5)
				panic(errR)
			} else {
				sentry.Flush(time.Second * 5)
			}
		}()

		utils.LogInfo("successful connect to the Sentry!")
	}

	if cfg.GrayLog.IsRun {
		gelfWriter, errW := gelf.NewWriter(cfg.GrayLog.URL)
		if errW != nil {
			utils.LogFatalError("gelf.NewWriter: %s", errW)
		}
		// log to both stderr and graylog2
		log.SetOutput(io.MultiWriter(os.Stderr, gelfWriter))
		utils.LogInfo("GrayLog successfully run!")
	}

	var pool *pgxpool.Pool
	pool, err = pgxpool.Connect(ctx, cfg.DatabaseDsn)
	if err != nil {
		utils.LogFatalError("Can't connect to the database server: %s", err)
	} else {
		psqlVer := ""
		_ = pool.QueryRow(ctx, "SELECT current_setting('server_version')").Scan(&psqlVer)
		utils.LogError("successful connect to the PostgresSQL %s \n", psqlVer)
	}
	defer pool.Close()

	userPermissionRepository := factory.NewUserPermissionRepository(ctx, pool)
	userRepository := factory.NewUserRepository(ctx, pool, userPermissionRepository)
	userService := factory2.NewUserAuthHandler(userRepository, *cfg)
	isUpdated, err := userService.UpdateDefaultAdminTokenByCfg(cfg.Auth)
	if err != nil {
		utils.LogFatalError("Can't update default Admin token: %v", err)
	} else if isUpdated {
		log.Println("Successfully updated default Admin token!")
	}

	licenseRepository := factory.NewLicenseRepository(ctx, pool)
	//err = handlers.InitCallbackCronJob(cfg.Callback, licenseRepository)
	//if err != nil {
	//	fmt.Println("i can't run send callback job: " + err.Error())
	//}

	routeParameters :=
		server.RouteParameters{
			Config:                   *cfg,
			UserRepository:           userRepository,
			UserActionRepository:     factory.NewUserActionRepository(ctx, pool),
			UserPermissionRepository: userPermissionRepository,
		}
	licenseRouteParameters :=
		handlers.LicenseRouteParameters{
			Config:             *cfg,
			CustomerRepository: factory.NewCustomerRepository(pool, licenseRepository),
			LicenseRepository:  licenseRepository,
		}
	handler := server.GetRouters(routeParameters, licenseRouteParameters)
	log.Fatal(http.ListenAndServe(cfg.ServerAddress, handler))
}
