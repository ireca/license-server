SELECT 'CREATE DATABASE license_server'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'license_server')\gexec

-- CREATE DATABASE license_server ENCODING = 'UTF8' ;

ALTER DATABASE license_server OWNER TO postgres;