ALTER TABLE public.ln_customers
    ADD COLUMN data text NOT NULL DEFAULT '',
    ADD COLUMN license_key text NOT NULL DEFAULT '';

ALTER TABLE public.ln_licenses ADD COLUMN data text NOT NULL DEFAULT '';
ALTER TABLE public.ln_licenses ALTER COLUMN license_key TYPE text USING (COALESCE(license_key, ''));