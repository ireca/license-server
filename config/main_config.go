package config

import (
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env/v6"
	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
	"github.com/jinzhu/copier"
	"gitlab.com/ireca/license-server/internal/utils"
	"os"
)

type Config struct {
	ServerAddress string   `env:"SERVER_ADDRESS" json:"serverAddress" `
	BaseURL       string   `env:"BASE_URL" json:"baseURL" `
	DatabaseDsn   string   `env:"DATABASE_DSN" json:"databaseDsn" `
	Auth          Auth     `json:"auth"`
	Callback      Callback `json:"callback"`
	GrayLog       Graylog  `json:"grayLog"`
	Vault         Vault    `json:"vault"`
	Sentry        Sentry   `json:"sentry"`
}

type Auth struct {
	AdminAuthToken string `env:"ADMIN_AUTH_TOKEN" json:"adminAuthToken"`
}

type Callback struct {
	MaxAttempts     uint   `env:"CALLBACK_MAX_ATTEMPTS" json:"maxAttempts"`
	LimitUnitOfTime uint   `env:"CALLBACK_LIMIT_UNIT_OF_TIME" json:"limitUnitOfTime"`
	CronSpec        string `env:"CRON_SPEC" json:"cronSpec"`
}

type Graylog struct {
	IsRun bool   `env:"GRAY_LOG_IS_RUN" json:"isRun"`
	URL   string `env:"GRAY_LOG_URL" json:"url"`
}

type Vault struct {
	IsRun          bool   `env:"VAULT_LOG_IS_RUN" json:"isRun"`
	URL            string `env:"VAULT_LOG_URL" json:"url"`
	RequestTimeout uint   `env:"VAULT_LOG_REQUEST_TIMEOUT" json:"requestTimeout"`
	RoleId         string `env:"VAULT_ROLE_ID" json:"roleId"`
	SecretId       string `env:"VAULT_SECRET_ID" json:"secretId"`
	MountPath      string `env:"VAULT_MOUNT_PATH" json:"mountPath"`
}

type Sentry struct {
	IsRun              bool     `env:"SENTRY_IS_RUN" json:"isRun"`
	Dsn                string   `env:"SENTRY_DSN" json:"dsn"`
	Debug              bool     `env:"SENTRY_DEBUG" json:"debug"`
	AttachStacktrace   bool     `env:"SENTRY_ATTACH_STACKTRACE" json:"attachStacktrace"`
	SampleRate         float64  `env:"SENTRY_SAMPLE_RATE" json:"sampleRate"`
	EnableTracing      bool     `env:"SENTRY_ENABLE_TRACING" json:"enableTracing"`
	TracesSampleRate   float64  `env:"SENTRY_TRACES_SAMPLE_RATE" json:"tracesSampleRate"`
	IgnoreErrors       []string `json:"ignoreErrors"`
	IgnoreTransactions []string `json:"ignoreTransactions"`
	SendDefaultPII     bool     `env:"SENTRY_SEND_DEFAULT_PII" json:"sendDefaultPII"`
	ServerName         string   `env:"SENTRY_SERVER_NAME" json:"serverName"`
	Release            string   `env:"SENTRY_RELEASE" json:"release"`
	Dist               string   `env:"SENTRY_DIST" json:"dist"`
	Environment        string   `env:"SENTRY_ENVIRONMENT" json:"environment"`
	MaxBreadcrumbs     int      `env:"SENTRY_MAX_BREADCRUMBS" json:"maxBreadcrumbs"`
	MaxSpans           int      `env:"SENTRY_MAX_SPANS" json:"maxSpans"`
	MaxErrorDepth      int      `env:"SENTRY_MAX_ERROR_DEPTH" json:"maxErrorDepth"`
}

var cfg Config

func GetConfigSettings(configFromFile *Config) (*Config, error) {
	const (
		CallbackMaxAttempts     = 3
		CallbackLimitUnitOfTime = 50
	)

	if configFromFile != nil {
		err := copier.Copy(&cfg, &configFromFile)
		if err != nil {

			return nil, fmt.Errorf("I can't copy json config: %w", err)
		}
	}

	err := env.Parse(&cfg)
	if err != nil {
		return nil, fmt.Errorf("I can't parse env from config: %w", err)
	}

	if cfg.ServerAddress == "" {
		return nil, fmt.Errorf("unknowned server adress")
	}
	if cfg.DatabaseDsn == "" {
		return nil, fmt.Errorf("unknowned database connection string")
	}
	if cfg.Auth.AdminAuthToken == "" {
		return nil, fmt.Errorf("unknowned admin auth token")
	}

	if cfg.Callback.MaxAttempts == 0 {
		cfg.Callback.MaxAttempts = CallbackMaxAttempts
	}

	if cfg.Callback.LimitUnitOfTime == 0 {
		cfg.Callback.LimitUnitOfTime = CallbackLimitUnitOfTime
	}

	if cfg.Callback.CronSpec == "" {
		return nil, fmt.Errorf("unknowned cron spec")
	}

	return &cfg, nil
}

// LoadConfigFile this method read a server configurations from a file in the json format.
func LoadConfigFile(configFilePath string) (*Config, error) {
	var configFromFile Config

	file, err := os.OpenFile(configFilePath, os.O_RDONLY, 0777)
	if err != nil {
		return nil, fmt.Errorf("unable to open a configuration file: %w", err)
	}
	defer utils.ResourceClose(file)

	info, err := file.Stat()
	if err != nil {
		return nil, fmt.Errorf("unable to get statistic info about configuration file: %w", err)
	}
	filesize := info.Size()
	jsonConfig := make([]byte, filesize)

	_, err = file.Read(jsonConfig)
	if err != nil {
		return nil, fmt.Errorf("i can't read a configuration file: %w", err)
	}

	err = json.Unmarshal(jsonConfig, &configFromFile)
	if err != nil {
		return nil, fmt.Errorf("i can't parse a configuration json file: %w", err)
	}

	return &configFromFile, nil
}

func GetConfigByVault(cfg Config, vaultValues *vault.Response[schema.KvV2ReadResponse]) (*Config, error) {
	for key, val := range vaultValues.Data.Data {
		if key == "databaseDsn" {
			cfg.DatabaseDsn = val.(string)
		} else if key == "adminAuthToken" {
			cfg.Auth.AdminAuthToken = val.(string)
		}
	}

	return &cfg, nil
}
