module gitlab.com/ireca/license-server

go 1.22.5

require (
	github.com/bxcodec/faker/v3 v3.8.1
	github.com/caarlos0/env/v6 v6.9.1
	github.com/getsentry/sentry-go v0.29.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-playground/validator/v10 v10.11.1
	github.com/google/uuid v1.6.0
	github.com/jackc/pgx/v4 v4.15.0
	github.com/jinzhu/copier v0.3.5
	github.com/robfig/cron/v3 v3.0.1
	github.com/stretchr/testify v1.8.2
	golang.org/x/net v0.23.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/hashicorp/go-rootcerts v1.0.2 // indirect
	github.com/hashicorp/go-secure-stdlib/strutil v0.1.2 // indirect
	github.com/hashicorp/vault-client-go v0.4.3 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.11.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.2.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.10.0 // indirect
	github.com/jackc/puddle v1.2.1 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/ryanuber/go-glob v1.0.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.6.0 // indirect
	gopkg.in/Graylog2/go-gelf.v1 v1.0.0-20170811154226-7ebf4f536d8f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
