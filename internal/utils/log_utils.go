package utils

import (
	"fmt"
	"github.com/getsentry/sentry-go"
	"io"
	"log"
)

func LogFatalError(format string, v ...any) {
	sentry.WithScope(func(scope *sentry.Scope) {
		scope.SetLevel(sentry.LevelFatal)
		err := fmt.Errorf(format, v)
		sentry.CaptureMessage(err.Error())
	})
	log.Fatalf(format, v)
}

func LogError(format string, v ...any) {
	sentry.WithScope(func(scope *sentry.Scope) {
		scope.SetLevel(sentry.LevelError)
		err := fmt.Errorf(format, v)
		sentry.CaptureMessage(err.Error())
	})
	log.Printf(format, v)
}

func LogInfo(mess string) {
	sentry.CaptureMessage(mess)
	log.Println(mess)
}

func LogErr(n int, err error) int {
	if err != nil {
		log.Printf("Write failed: %v", err)
	}

	return n
}

func ResourceClose(body io.ReadCloser) {
	if err := body.Close(); err != nil {
		log.Printf("Can't close resource: %v", err)
	}
}
