package server

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/ireca/license-server/internal/handlers"
	"net/http"
)

func getCreateLicenseRoute(r *chi.Mux, params handlers.LicenseRouteParameters) *chi.Mux {
	r.Post("/api/v1/licenses/create", func(w http.ResponseWriter, r *http.Request) {
		handlers.CreateLicense(r, w, params)
	})

	return r
}

func getReplaceLicenseRoute(r *chi.Mux, params handlers.LicenseRouteParameters) *chi.Mux {
	r.Post("/api/v1/licenses/replace", func(w http.ResponseWriter, r *http.Request) {
		handlers.ReplacedLicense(r, w, params)
	})

	return r
}

func getDeleteLicenseRoute(r *chi.Mux, params handlers.LicenseRouteParameters) *chi.Mux {
	r.Delete("/api/v1/licenses", func(w http.ResponseWriter, r *http.Request) {
		handlers.DeletedLicense(r, w, params)
	})

	return r
}

func getLicenseRoute(r *chi.Mux, params handlers.LicenseRouteParameters) *chi.Mux {
	r.Get("/api/v1/license", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetLicense(r, w, params)
	})

	return r
}
