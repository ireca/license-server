package server

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/bxcodec/faker/v3"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ireca/license-server/config"
	"gitlab.com/ireca/license-server/internal/handlers"
	"gitlab.com/ireca/license-server/internal/repositories/factory"
	"gitlab.com/ireca/license-server/internal/utils"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

type LicenseTest struct {
	LicenseId    string `faker:"uuid_hyphenated" json:"licenseId,omitempty"`
	ProductType  string `faker:"oneof: courier, waiter, pechka54, solo" json:"productType,omitempty"`
	CallbackURL  string `faker:"url" json:"callbackUrl,omitempty"`
	Count        int    `faker:"boundary_start=1, boundary_end=100" json:"count,omitempty"`
	LicenseKey   string `faker:"uuid_hyphenated" json:"licenseKey,omitempty"`
	ActivationAt string `faker:"timestamp" json:"activationAt,omitempty"`
	ExpirationAt string `faker:"timestamp" json:"expirationAt,omitempty"`
	Data         string `faker:"len=1024" json:"data,omitempty"`
	Description  string `faker:"len=256" json:"description,omitempty"`
}

type CustomerTest struct {
	CustomerId  string         `faker:"uuid_hyphenated" json:"customerId,omitempty"`
	Type        string         `faker:"oneof: device, service" json:"type,omitempty"`
	Inn         string         `faker:"-" json:"inn,omitempty"`
	Title       string         `faker:"username" json:"title,omitempty"`
	LicenseKey  string         `faker:"uuid_hyphenated" json:"licenseKey,omitempty"`
	Data        string         `faker:"len=1024" json:"data,omitempty"`
	Description string         `faker:"len=256" json:"description,omitempty"`
	Licenses    [1]LicenseTest `json:"Licenses,omitempty"`
}

func TestCRUDLicenseRouters(t *testing.T) {
	licenseTests := []struct {
		url     string
		request CustomerTest
	}{
		{
			url: "/api/v1/licenses/replace",
			request: CustomerTest{
				CustomerId:  "",
				Type:        "",
				Inn:         "",
				Title:       "",
				LicenseKey:  "",
				Data:        "",
				Description: "",
				Licenses: [1]LicenseTest{{
					LicenseId:    "",
					ProductType:  "",
					CallbackURL:  "",
					Count:        0,
					LicenseKey:   "",
					ActivationAt: "",
					ExpirationAt: "",
					Data:         "",
					Description:  "",
				}},
			},
		},
		{
			url: "/api/v1/licenses/create",
			request: CustomerTest{
				CustomerId:  "",
				Type:        "",
				Inn:         "",
				Title:       "",
				LicenseKey:  "",
				Data:        "",
				Description: "",
				Licenses: [1]LicenseTest{{
					LicenseId:    "",
					ProductType:  "",
					CallbackURL:  "",
					Count:        0,
					LicenseKey:   "",
					ActivationAt: "",
					ExpirationAt: "",
					Data:         "",
					Description:  "",
				}},
			},
		},
	}

	configPath, err := GetConfigPath()
	if err != nil {
		log.Fatalf("i can't get path to the configuration file:" + err.Error())
	}

	configFromFile, err := config.LoadConfigFile(configPath)
	if err != nil {
		log.Fatalf("i can't load configuration file:" + err.Error())
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		log.Fatalf("Can't read cfg: %s", err.Error())
	}

	var pool *pgxpool.Pool
	ctx := context.Background()

	pool, _ = pgxpool.Connect(ctx, cfg.DatabaseDsn)
	defer pool.Close()
	userPermissionRepository := factory.NewUserPermissionRepository(ctx, pool)
	routeParameters :=
		RouteParameters{
			Config:                   *cfg,
			UserRepository:           factory.NewUserRepository(ctx, pool, userPermissionRepository),
			UserActionRepository:     factory.NewUserActionRepository(ctx, pool),
			UserPermissionRepository: userPermissionRepository,
		}

	licenseRepository := factory.NewLicenseRepository(ctx, pool)
	licenseRouteParameters :=
		handlers.LicenseRouteParameters{
			Config:             *cfg,
			CustomerRepository: factory.NewCustomerRepository(pool, licenseRepository),
			LicenseRepository:  licenseRepository,
		}
	r := GetRouters(routeParameters, licenseRouteParameters)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tt := range licenseTests {
		errF := faker.FakeData(&tt.request)
		if errF != nil {
			log.Fatal(err)
		}
		tt.request.Inn = "1234567890"

		request, errC := getCustomerRequest(tt.request)
		assert.NoError(t, errC)

		jsonRequest := getPostLicenseRequest(t, ts, tt.url, strings.NewReader(string(request)), cfg.Auth.AdminAuthToken)
		resp, _ := sendLicenseRequest(t, jsonRequest)
		assert.Equal(t, 201, resp.StatusCode)
		if errR := resp.Body.Close(); errR != nil {
			log.Fatal(errR)
		}
	}

}

func getCustomerRequest(request CustomerTest) ([]byte, error) {
	jsonResp, err := json.Marshal(request)
	if err != nil {
		return []byte(""), fmt.Errorf("i can't decode json request: %w", err)
	}

	return jsonResp, nil
}

func getPostLicenseRequest(t *testing.T, ts *httptest.Server, path string, body io.Reader, token string) *http.Request {
	req, err := http.NewRequest("POST", ts.URL+path, body)
	require.NoError(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	require.NoError(t, err)

	return req
}

func sendLicenseRequest(t *testing.T, req *http.Request) (*http.Response, string) {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	resp, err := client.Do(req)
	require.NoError(t, err)

	respBody, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)

	defer utils.ResourceClose(resp.Body)

	return resp, string(respBody)
}

func GetConfigPath() (string, error) {
	f, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("can't get file config path: %w", err)
	}

	return filepath.Dir(f) + "/../config.json", nil
}
