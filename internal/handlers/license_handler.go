package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/jinzhu/copier"
	"gitlab.com/ireca/license-server/config"
	"gitlab.com/ireca/license-server/internal/handlers/auth"
	"gitlab.com/ireca/license-server/internal/models"
	"gitlab.com/ireca/license-server/internal/repositories/interfaces"
	"gitlab.com/ireca/license-server/internal/utils"
	"log"
	"net/http"
	"strings"
)

type LicenseRouteParameters struct {
	Config             config.Config
	CustomerRepository interfaces.CustomerRepository
	LicenseRepository  interfaces.LicenseRepository
}

type LicenseRequest struct {
	LicenseId    string `validate:"required,max=64"`
	ProductType  string `validate:"required, required,oneof='courier' 'solo' 'pechka54'"`
	CallbackURL  string `validate:"url,max=500"`
	Count        int    `validate:"required,number"`
	LicenseKey   string `validate:"-"`
	ActivationAt string `validate:"required,datetime"`
	ExpirationAt string `validate:"required,datetime"`
	Data         string `validate:"-"`
	Duration     int    `validate:"number"`
	Description  string `validate:"max=256"`
}

type CustomerRequest struct {
	CustomerId  string `validate:"required,max=64"`
	Type        string `validate:"required,oneof='service' 'device'"`
	Inn         string `validate:"max=12"`
	Title       string `validate:"required,max=100"`
	LicenseKey  string `validate:"-"`
	Data        string `validate:"-"`
	Description string `validate:"max=256"`
	Licenses    []LicenseRequest
}

func CreateLicense(r *http.Request, w http.ResponseWriter, param LicenseRouteParameters) {
	httpRequest, err := getCreatedRequest(r)
	log.Printf("create_license_request: %v", httpRequest)
	if err != nil {
		utils.LogError("i can't decode create license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	err = validate.Struct(httpRequest)
	if err != nil {
		utils.LogError("i can't validate create license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	customerID := 0
	userAuth := r.Context().Value("userAuth").(*auth.UserAuth)
	customer, err := param.CustomerRepository.FindByCode(r.Context(), userAuth.User.ID, httpRequest.CustomerId)
	if err != nil {
		utils.LogError("i cant validate create license request: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if customer == nil {
		var customerNew models.Customer
		err = copier.Copy(&customerNew, &httpRequest)
		if err != nil {
			utils.LogError("i can't copy request: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		customerNew.UserID = userAuth.User.ID
		customerNew.Code = httpRequest.CustomerId
		customerID, err = param.CustomerRepository.Create(r.Context(), userAuth.User.ID, customerNew)
		if err != nil {
			utils.LogError("i can't create customer: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		customerID = customer.ID
	}

	licenses, err := getCustomerLicenses(httpRequest)
	if err != nil {
		utils.LogError("i can't get customer: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = param.LicenseRepository.MultipleInsert(customerID, licenses)
	if err != nil {
		utils.LogError("i can't create licenses: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var responses []LicenseResponse
	for _, license := range licenses {
		responses = append(responses, LicenseResponse{LicenseId: license.Code, LicenseKey: license.LicenseKey})
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	jsonResponse, err := getCreateResponse(responses)
	if err != nil {
		utils.LogError("i can't get response: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.LogErr(w.Write(jsonResponse))
}

func ReplacedLicense(r *http.Request, w http.ResponseWriter, param LicenseRouteParameters) {
	httpRequest, err := getCreatedRequest(r)
	log.Printf("replace_license_request: %v", httpRequest)
	if err != nil {
		utils.LogError("i can't decode create license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	err = validate.Struct(httpRequest)
	if err != nil {
		utils.LogError("i can't decode create license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	licenses, err := getCustomerLicenses(httpRequest)
	if err != nil {
		utils.LogError("i can't get customer: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	userAuth := r.Context().Value("userAuth").(*auth.UserAuth)

	var customer models.Customer
	err = copier.Copy(&customer, &httpRequest)
	if err != nil {
		utils.LogError("i can't copy license: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	customer.UserID = userAuth.User.ID
	customer.Code = httpRequest.CustomerId
	err = param.CustomerRepository.Replace(r.Context(), userAuth.User.ID, customer, licenses)
	if err != nil {
		utils.LogError("i can't replace license: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var responses []LicenseResponse
	for _, license := range licenses {
		responses = append(responses, LicenseResponse{LicenseId: license.Code, LicenseKey: license.LicenseKey})
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	jsonResponse, err := getCreateResponse(responses)
	if err != nil {
		utils.LogError("i can't get response: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.LogErr(w.Write(jsonResponse))
}

func getCustomerLicenses(httpRequest *CustomerRequest) ([]models.Licence, error) {
	var licences []models.Licence
	for _, licenseRequest := range httpRequest.Licenses {
		activationAt, err := utils.GetTimeFromStr(licenseRequest.ActivationAt)
		if err != nil {
			return nil, err
		}

		expirationAt, err := utils.GetTimeFromStr(licenseRequest.ExpirationAt)
		if err != nil {
			return nil, err
		}

		licenseKey := licenseRequest.LicenseKey
		if licenseKey == "" {
			licenseKey, err = getLicenseKey()
			if err != nil {
				return nil, err
			}
		}

		var license models.Licence
		err = copier.Copy(&license, &licenseRequest)
		if err != nil {
			return licences, err
		}
		license.Code = licenseRequest.LicenseId
		license.LicenseKey = licenseKey
		license.ActivationAt = activationAt
		license.ExpirationAt = expirationAt
		license.Duration = licenseRequest.Duration
		licences = append(licences, license)
	}

	return licences, nil
}

func getLicenseKey() (string, error) {
	uuidWithHyphen, err := uuid.NewRandom()
	if err != nil {
		return "", nil
	}
	v := strings.Replace(uuidWithHyphen.String(), "-", "", -1)

	return v, nil
}

func getCreatedRequest(r *http.Request) (*CustomerRequest, error) {
	var request CustomerRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)
	if err != nil {
		return nil, fmt.Errorf("i can't decode json request: %w", err)
	}

	return &request, nil
}

type LicenseResponse struct {
	LicenseId  string `json:"licenseId"`
	LicenseKey string `json:"licenseKey"`
}

func getCreateResponse(responses []LicenseResponse) ([]byte, error) {
	jsonResp, err := json.Marshal(responses)
	if err != nil {
		return jsonResp, fmt.Errorf("error happened in JSON marshal: %w", err)
	}

	return jsonResp, nil
}

type LicenseDeleteRequest struct {
	CustomerId string `validate:"required,max=64"`
	LicenseId  string `validate:"required,max=64"`
}

func DeletedLicense(r *http.Request, w http.ResponseWriter, param LicenseRouteParameters) {
	httpRequest, err := getDeleteLicenseRequest(r)
	log.Printf("delete_license_request: %v", httpRequest)
	if err != nil {
		utils.LogError("i can't decode delete license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	err = validate.Struct(httpRequest)
	if err != nil {
		utils.LogError("i can't validate delete license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userAuth := r.Context().Value("userAuth").(*auth.UserAuth)

	customer, err := param.CustomerRepository.FindByCode(r.Context(), userAuth.User.ID, httpRequest.CustomerId)
	if err != nil {
		utils.LogError("i can't get customer: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if customer == nil {
		http.Error(
			w,
			fmt.Errorf("this customer isn't exist, customerId=%s", httpRequest.CustomerId).Error(),
			http.StatusInternalServerError,
		)
		return
	}

	err = param.LicenseRepository.Delete(customer.ID, httpRequest.LicenseId)
	if err != nil {
		utils.LogError("i can't delete a license: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func getDeleteLicenseRequest(r *http.Request) (*LicenseDeleteRequest, error) {
	var request LicenseDeleteRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)
	if err != nil {
		return nil, fmt.Errorf("i can't decode json request: %w", err)
	}

	return &request, nil
}

type CustomerGetRequest struct {
	CustomerId  string `validate:"required,min=1,max=64"`
	LicenseId   string `validate:"max=64"`
	ProductType string `validate:"max=20"`
}

func GetLicense(r *http.Request, w http.ResponseWriter, param LicenseRouteParameters) {
	httpReq := getLicenseRequest(r)
	log.Printf("get_license_request: %v", httpReq)
	validate := validator.New()
	err := validate.Struct(httpReq)
	if err != nil {
		utils.LogError("i can't validate a license request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userAuth := r.Context().Value("userAuth").(*auth.UserAuth)
	customer, err := param.CustomerRepository.FindFull(r.Context(), userAuth.User.ID, httpReq.CustomerId, httpReq.LicenseId, httpReq.ProductType)
	if err != nil {
		utils.LogError("i can't get licenses: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if customer == nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	customerRes, err := getCustomerResponse(*customer)
	if err != nil {
		utils.LogError("i can't get customer response: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonResponse, err := getLicenseJsonResponse(customerRes)
	if err != nil {
		utils.LogError("i can't decode create license request: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.LogErr(w.Write(jsonResponse))
}

func getLicenseRequest(r *http.Request) *CustomerGetRequest {
	var request CustomerGetRequest
	request.CustomerId = r.URL.Query().Get("customerId")
	request.LicenseId = r.URL.Query().Get("licenseId")
	request.ProductType = r.URL.Query().Get("productType")

	return &request
}

type LicenseGetResponse struct {
	LicenseId    string `json:"licenseId"`
	ProductType  string `json:"productType"`
	CallbackURL  string `json:"callbackUrl"`
	Count        int    `json:"count"`
	LicenseKey   string `json:"licenseKey"`
	ActivationAt string `json:"activationAt"`
	ExpirationAt string `json:"expirationAt"`
	Data         string `json:"data"`
	Duration     int    `json:"duration"`
	Description  string `json:"description"`
}

type CustomerGetResponse struct {
	CustomerId  string               `json:"customerId"`
	Type        string               `json:"type"`
	Inn         string               `json:"inn"`
	Title       string               `json:"title"`
	LicenseKey  string               `json:"licenseKey"`
	Data        string               `json:"data"`
	Description string               `json:"description"`
	Licenses    []LicenseGetResponse `json:"licenses" copier:"-"`
}

func getCustomerResponse(user models.Customer) (CustomerGetResponse, error) {
	var customers = make(map[int]models.Customer)
	customers[user.ID] = user
	responses, err := getCustomerResponses(&customers)
	if err != nil {
		return CustomerGetResponse{}, err
	}

	for _, response := range responses {
		return response, err
	}

	return CustomerGetResponse{}, nil
}

func getCustomerResponses(customers *map[int]models.Customer) ([]CustomerGetResponse, error) {
	var responses []CustomerGetResponse
	for _, customer := range *customers {
		var response CustomerGetResponse
		err := copier.Copy(&response, &customer)
		if err != nil {
			return nil, err
		}
		response.CustomerId = customer.Code
		var licenseResponse LicenseGetResponse
		for _, license := range customer.Licenses {
			err = copier.Copy(&licenseResponse, &license)
			if err != nil {
				return nil, err
			}
			licenseResponse.LicenseId = license.Code
			licenseResponse.ActivationAt = license.ActivationAt.Format("2006-01-02 15:04:05")
			licenseResponse.ExpirationAt = license.ExpirationAt.Format("2006-01-02 15:04:05")
			response.Licenses = append(response.Licenses, licenseResponse)
		}
		responses = append(responses, response)
	}

	return responses, nil
}

func getLicenseJsonResponse(resp CustomerGetResponse) ([]byte, error) {
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		return jsonResp, fmt.Errorf("error happened in JSON marshal: %w", err)
	}

	return jsonResp, nil
}
