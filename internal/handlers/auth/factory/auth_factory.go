package factory

import (
	"gitlab.com/ireca/license-server/config"
	"gitlab.com/ireca/license-server/internal/handlers/auth"
	repositoryInterfaces "gitlab.com/ireca/license-server/internal/repositories/interfaces"
)

func NewUserAuthHandler(userRepository repositoryInterfaces.UserRepository, config config.Config) *auth.UserAuthHandler {
	return auth.NewUserAuth(userRepository, config)
}
