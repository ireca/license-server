package auth

import (
	"fmt"
	"gitlab.com/ireca/license-server/config"
	"gitlab.com/ireca/license-server/internal/models"
	"gitlab.com/ireca/license-server/internal/repositories/interfaces"
	"net/http"
	"strings"
)

const Admin = 1
const User = 2

const defaultAdminToken = "54d1ba805e2a4891aeac9299b618945e"

type UserAuth struct {
	Role int
	User models.User
}

type UserAuthHandler struct {
	userRepository interfaces.UserRepository
	config         config.Config
}

func NewUserAuth(userRepository interfaces.UserRepository, config config.Config) *UserAuthHandler {
	return &UserAuthHandler{userRepository, config}
}

func (u UserAuthHandler) GetAuthUser(token string) (*UserAuth, error) {
	if len(token) < 32 {
		return nil, fmt.Errorf("User doesn't have correct a token")
	}

	user, err := u.userRepository.FindByToken(token)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, fmt.Errorf("User not found")
	}

	if user.AuthToken == u.config.Auth.AdminAuthToken && token == user.AuthToken {
		return &UserAuth{Role: Admin, User: *user}, nil
	}

	return &UserAuth{Role: User, User: *user}, nil
}

func (u UserAuthHandler) UpdateDefaultAdminTokenByCfg(cfg config.Auth) (bool, error) {
	if len(cfg.AdminAuthToken) < 32 {
		return false, fmt.Errorf("admin auth token less then 32 chars in your config")
	}

	user, err := u.userRepository.FindByToken(defaultAdminToken)
	if err != nil {
		return false, fmt.Errorf("can't find user by default admin token: %v", err)
	}

	if user == nil {
		return false, nil
	}
	user.AuthToken = cfg.AdminAuthToken

	return true, u.userRepository.UpdateDefaultToken(user.AuthToken)
}

func (u UserAuthHandler) GetToken(r *http.Request) string {
	token := r.Header.Get("Authorization")
	token = strings.Replace(token, "Bearer", "", 1)

	return strings.ReplaceAll(token, " ", "")
}
