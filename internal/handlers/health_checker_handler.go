package handlers

import (
	"fmt"
	"net/http"
)

// HealthCheckerHandler check health of the service.
func HealthCheckerHandler(r *http.Request, w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	_, err := fmt.Fprintf(w, "Service is healthy\n")
	if err != nil {
		fmt.Printf("can't print message: %v\n", err)
	}
}
