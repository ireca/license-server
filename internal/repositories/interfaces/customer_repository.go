package interfaces

import (
	"context"
	"gitlab.com/ireca/license-server/internal/models"
)

type CustomerRepository interface {
	Create(ctx context.Context, userId int, customer models.Customer) (customerId int, err error)
	Replace(ctx context.Context, userId int, model models.Customer, licenses []models.Licence) error
	Delete(ctx context.Context, userId int, code string) error
	FindByCode(ctx context.Context, userId int, code string) (*models.Customer, error)
	IsInDatabase(ctx context.Context, userId int, code string) (bool, error)
	FindFull(ctx context.Context, userId int, customerCode string, licenseCode string, productType string) (*models.Customer, error)
}
