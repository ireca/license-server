package interfaces

import "gitlab.com/ireca/license-server/internal/models"

type UserPermissionRepository interface {
	MultipleInsert(userId int, models []models.UserPermission) error
	FindALL(userId int) ([]models.UserPermission, error)
	Delete(userId int) error
}
