package interfaces

import "gitlab.com/ireca/license-server/internal/models"

type UserActionRepository interface {
	FindALL() (map[string]models.UserAction, error)
}
