package factory

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/license-server/internal/repositories/interfaces"
	"gitlab.com/ireca/license-server/internal/repositories/postgre"
)

func NewCustomerRepository(pool *pgxpool.Pool, licenseRep interfaces.LicenseRepository) interfaces.CustomerRepository {
	return postgre.NewCustomerRepository(pool, licenseRep)
}

func NewLicenseRepository(context context.Context, pool *pgxpool.Pool) interfaces.LicenseRepository {
	return postgre.NewLicenseRepository(context, pool)
}
