package factory

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/license-server/internal/repositories/interfaces"
	"gitlab.com/ireca/license-server/internal/repositories/postgre"
)

func NewUserRepository(context context.Context, pool *pgxpool.Pool, permissionRep interfaces.UserPermissionRepository) interfaces.UserRepository {
	return postgre.NewUserRepository(context, pool, permissionRep)
}

func NewUserPermissionRepository(context context.Context, pool *pgxpool.Pool) interfaces.UserPermissionRepository {
	return postgre.NewUserPermissionRepository(context, pool)
}

func NewUserActionRepository(context context.Context, pool *pgxpool.Pool) interfaces.UserActionRepository {
	return postgre.NewUserActionRepository(context, pool)
}
