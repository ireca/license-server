package postgre

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/license-server/internal/models"
	"gitlab.com/ireca/license-server/internal/repositories/interfaces"
)

type customerRepository struct {
	connection *pgxpool.Pool
	licenseRep interfaces.LicenseRepository
}

func NewCustomerRepository(pool *pgxpool.Pool, licenseRep interfaces.LicenseRepository) interfaces.CustomerRepository {
	return &customerRepository{pool, licenseRep}
}

func (c customerRepository) Create(ctx context.Context, userId int, customer models.Customer) (customerId int, err error) {
	err = c.connection.QueryRow(
		ctx,
		`INSERT INTO 
			ln_customers(user_id, code, type, title, inn, data, license_key, description) 
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			ON CONFLICT (user_id, code) DO UPDATE 
			SET
			  type = excluded.type,
			  title = excluded.title,
			  inn = excluded.inn,
			  data = excluded.data,
			  license_key = excluded.license_key,
			  description = excluded.description
			RETURNING id`,
		userId, &customer.Code, &customer.Type, &customer.Title, &customer.Inn, &customer.Data, &customer.LicenseKey, &customer.Description,
	).Scan(&customerId)
	if err != nil {
		return 0, err
	}

	return customerId, nil
}

func (c customerRepository) Replace(ctx context.Context, userId int, customer models.Customer, licenses []models.Licence) error {
	tx, err := c.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}
	defer func() error {
		if err != nil {
			return tx.Rollback(ctx)
		} else {
			return tx.Commit(ctx)
		}
	}()

	var customerId int
	err = c.connection.QueryRow(
		ctx,
		`INSERT INTO 
			ln_customers(user_id, code, type, title, inn, data, license_key, description) 
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			ON CONFLICT (user_id, code) DO UPDATE 
			SET
			  type = excluded.type,
			  title = excluded.title,
			  inn = excluded.inn,
			  data = excluded.data,
			  license_key = excluded.license_key,
			  description = excluded.description
			RETURNING id`,
		userId, &customer.Code, &customer.Type, &customer.Title, &customer.Inn, &customer.Data, &customer.LicenseKey, &customer.Description,
	).Scan(&customerId)
	if err != nil {
		return err
	}

	err = c.licenseRep.DeleteAll(customerId)
	if err != nil {
		return err
	}

	err = c.licenseRep.MultipleReplace(customerId, licenses)

	return err
}

func (c customerRepository) Delete(ctx context.Context, userId int, code string) error {
	//TODO implement me
	panic("implement me")
}

func (c customerRepository) FindFull(ctx context.Context, userId int, customerCode string, licenseCode string, productType string) (*models.Customer, error) {
	var err error
	var rows pgx.Rows
	sqlBody :=
		`SELECT 
  		  c.id, c.user_id, c.code, c.type, c.title, c.inn, c.data, c.license_key, c.description,
  		  l.product_type, l.callback_url, l.count, l.license_key, l.registration_at, 
  		  l.activation_at, l.expiration_at, l.duration, l.data, l.description, l.code
		FROM 
  		  ln_customers c
		LEFT JOIN ln_licenses l ON l.customer_id=c.id`

	if licenseCode != "" && productType != "" {
		rows, err = c.connection.Query(
			ctx,
			sqlBody+` WHERE c.user_id=$1 AND c.code=$2 AND l.code=$3 AND l.product_type=$4 AND c.deleted_at IS NULL`,
			userId, customerCode, licenseCode, productType,
		)
	} else if licenseCode != "" && productType == "" {
		rows, err = c.connection.Query(
			ctx,
			sqlBody+` WHERE c.user_id=$1 AND c.code=$2 AND l.code=$3 AND c.deleted_at IS NULL`,
			userId, customerCode, licenseCode,
		)
	} else if productType != "" && licenseCode == "" {
		rows, err = c.connection.Query(
			ctx,
			sqlBody+` WHERE c.user_id=$1 AND c.code=$2 AND l.product_type=$3 AND c.deleted_at IS NULL`,
			userId, customerCode, productType,
		)
	} else {
		rows, err = c.connection.Query(
			ctx,
			sqlBody+` WHERE c.user_id=$1 AND c.code=$2 AND c.deleted_at IS NULL`,
			userId, customerCode,
		)
	}
	if err != nil {
		return nil, err
	}

	customers, err := getCustomerModels(rows)
	if err != nil {
		return nil, err
	}

	for _, customer := range customers {
		return &customer, nil
	}

	return nil, nil
}

func getCustomerModels(rows pgx.Rows) (map[int]models.Customer, error) {
	var customers = make(map[int]models.Customer)
	var model models.Customer
	var licence models.Licence
	var customer = models.Customer{}
	lastUserId := 0
	for rows.Next() {
		err := rows.Scan(
			&model.ID, &model.UserID, &model.Code, &model.Type, &model.Title, &model.Inn, &model.Data, &model.LicenseKey, &model.Description,
			&licence.ProductType, &licence.CallbackURL, &licence.Count, &licence.LicenseKey, &licence.RegistrationAt,
			&licence.ActivationAt, &licence.ExpirationAt, &licence.Duration, &licence.Data, &licence.Description, &licence.Code,
		)
		if err != nil {
			return nil, err
		}

		if lastUserId != model.ID {
			customer = models.Customer{}
			customer.ID = model.ID
			customer.UserID = model.UserID
			customer.Code = model.Code
			customer.Title = model.Title
			customer.Type = model.Type
			customer.Inn = model.Inn
			customer.LicenseKey = model.LicenseKey
			customer.Data = model.Data
			customer.Description = model.Description
		}
		lastUserId = model.ID

		customer.Licenses = append(
			customer.Licenses,
			models.Licence{
				ID:             licence.ID,
				Code:           licence.Code,
				CustomerId:     licence.CustomerId,
				ProductType:    licence.ProductType,
				CallbackURL:    licence.CallbackURL,
				Count:          licence.Count,
				LicenseKey:     licence.LicenseKey,
				RegistrationAt: licence.RegistrationAt,
				ActivationAt:   licence.ActivationAt,
				ExpirationAt:   licence.ExpirationAt,
				Duration:       licence.Duration,
				Data:           licence.Data,
				Description:    licence.Description,
			},
		)

		customers[customer.ID] = customer
	}

	return customers, nil
}

func (c customerRepository) FindByCode(ctx context.Context, userId int, code string) (*models.Customer, error) {
	var model models.Customer
	err := c.connection.QueryRow(
		ctx,
		`SELECT 
			   id, user_id, code, type, title, inn, license_key, data, description
			 FROM 
			   ln_customers 
			 WHERE user_id=$1 AND code=$2 AND deleted_at IS NULL`,
		userId, code,
	).Scan(&model.ID, &model.UserID, &model.Code, &model.Type, &model.Title, &model.Inn, &model.LicenseKey, &model.Data, &model.Description)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &model, nil
}

func (c customerRepository) IsInDatabase(ctx context.Context, userId int, code string) (bool, error) {
	model, err := c.FindByCode(ctx, userId, code)

	return !(model == nil), err
}
